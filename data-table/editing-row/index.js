import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import onClickOutside from 'react-onclickoutside';
import isEmpty from 'lodash/isEmpty';
import isFunction from 'lodash/isFunction';
import pick from 'lodash/pick';
import Select from '../../elements/select';
import CleaveInput from '../../elements/cleaveInput';
import AutocompleteInput from '../../elements/autocompleteInput';
import Button from '../../elements/button';
import CostMark from '../../elements/costMark';
import DropdownList from '../../elements/dropdownList';
import { PAYMENT, MATERIAL, MECHANISM, INVOICE } from '../../../api/resource-types';
import { formatToPrice, formatToText } from '../../elements/cleaveInput/format';

import styles from '../styles.less';

class EditingRow extends Component {
  static propTypes = {
    row: PropTypes.object.isRequired,
    columns: PropTypes.array.isRequired,
    options: PropTypes.object,
    onCreateOptions: PropTypes.object,
    removeRow: PropTypes.func.isRequired,
    onDataChange: PropTypes.func.isRequired,
    onSubmitEditing: PropTypes.func.isRequired,
    onExitEditing: PropTypes.func.isRequired,
    getValueFromAutocompleteOption: PropTypes.func.isRequired,
    edit: PropTypes.bool,
  };

  static defaultProps = {
    options: {},
    onCreateOptions: {},
    edit: false,
  };

  constructor(props) {
    super(props);

    const { columns, row } = props;
    const values = pick(row, columns.map(column => column.accessor));
    columns
      .filter(column => column.calculate)
      .forEach(column => {
        values[column.accessor] = values[column.accessor] || 0;
      });

    this.state = {
      dropdown: false,
      typeSelected: null,
      errors: {},
      inputValues: {
        ...values,
      },
    };
  }

  componentDidMount() {
      document.addEventListener('keydown', this.handleDocumentKeyEnter);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleDocumentKeyEnter);
  }

  handleDocumentKeyEnter = e => {
    if (e.key === 'Enter') {
      this.handleClickOutside();
    }
  };

  handleClickOutside = e => {
    const { errors } = this.state;
    const { columns, row, onSubmitEditing } = this.props;

    if (!isEmpty(errors)) {
      e.preventDefault();
      e.stopPropagation();
      return;
    }

    const requiredFields = columns
      .filter(col => this.isColumnRequired(col))
      .map(col => col.accessor);
    let isValid = true;

    requiredFields.map(field => {
      if (row[field] === undefined || row[field] === null || Number(row[field]) === 0) {
        isValid = false;
        this.setState(prevState => ({
          errors: {
            ...prevState.errors,
            [field]: true, // means we have an error
          },
        }));
      }
      return field;
    });

    if (isValid) {
      onSubmitEditing();
    }
  };

  onInputChange = (col, value) => {
    const { onDataChange, columns } = this.props;
    const requiredFields = columns
      .filter(col => this.isColumnRequired(col))
      .map(col => col.accessor);
    const isEscapedValidationField = !requiredFields.some(field => field === col.accessor);

    if (!col.calculate) {
      onDataChange(col.accessor, value);
    }
    
    this.setState(prevState => {
      const inputValues = {
        ...prevState.inputValues,
        [col.accessor]: value,
      };

      if (col.dependent) {
        col.dependent.forEach(dependent => {
          const dependentColumn = columns.find(col => col.accessor === dependent);

          if (dependentColumn && this.isColumnEditable(dependentColumn)) {
            const dependentValue = dependentColumn.dependOnCalculate({...inputValues, changedCol: col});
            inputValues[dependentColumn.accessor] = dependentValue;
            if (!dependentColumn.calculate) {
              onDataChange(dependentColumn.accessor, dependentValue);
            }
          }
        });
      }

      return {
        inputValues,
      };
    });

    if (isEscapedValidationField || (value && (value.length > 0 || value >= 0))) {
      const newState = this.state.errors;
      delete newState[col.accessor];

      this.setState({
        errors: {
          ...newState,
        },
      });
    } else {
      this.setState(prevState => ({
        errors: {
          ...prevState.errors,
          [col.accessor]: true,
        },
      }));
    }
  };

  onSubmit = e => {
    e.preventDefault();
    this.handleDocumentKeyEnter(e);
    this.handleClickOutside(e);
    return false;
  };

  toggleDropdown = () => this.setState(prevState => ({ dropdown: !prevState.dropdown }));

  handleDropdownClick = type => () =>
    this.setState(
      {
        typeSelected: type,
      },
      () => this.props.onDataChange('type', type),
    );

  onAutocompleteOptionClick = (id, name) => {
    const { getValueFromAutocompleteOption, onDataChange, columns } = this.props;
    const values = getValueFromAutocompleteOption(id, name);

    Object.keys(values).forEach(key => {
      if (key === 'type') {
        onDataChange(key, values[key]);
      } else {
        this.onInputChange(columns.find(col => col.accessor === key), values[key]);
      }
    });
  };

  isColumnAutoComplete = col => {
    const { row } = this.props;

    if (isFunction(col.autocomplete)) {
      return col.autocomplete(row);
    }

    return !!col.autocomplete;
  };

  isColumnEditable = col => {
    const { row } = this.props;

    if (isFunction(col.editable)) {
      return col.editable(row);
    }

    return !!col.editable;
  };

  isColumnRequired = col => {
    const { row } = this.props;

    if (isFunction(col.required)) {
      return col.required(row);
    }

    return !!col.required;
  };

  renderBadge = row => {
    const { dropdown, typeSelected } = this.state;

    return (
      <div tabIndex={0} role="button" onClick={this.toggleDropdown}>
        <CostMark style={{ cursor: 'pointer' }} type={typeSelected || row.type} />
        {dropdown && (
          <DropdownList
            ref={el => {
              this.dropdown = el;
            }}
            style={{
              whiteSpace: 'nowrap',
              left: '-5px',
              top: 0,
              color: '#000',
              letterSpacing: 0,
              fontWeight: 'normal',
            }}
          >
            <DropdownList.Item onClick={this.handleDropdownClick(PAYMENT)}>
              <CostMark stateName="ТРД" />
              Оплата труда
            </DropdownList.Item>
            <DropdownList.Item onClick={this.handleDropdownClick(MATERIAL)}>
              <CostMark stateName="МАТ" />
              Материалы
            </DropdownList.Item>
            <DropdownList.Item onClick={this.handleDropdownClick(MECHANISM)}>
              <CostMark stateName="МЕХ" />
              Механизмы
            </DropdownList.Item>
            <DropdownList.Item onClick={this.handleDropdownClick(INVOICE)}>
              <CostMark stateName="НКЛ" />
              Накладные
            </DropdownList.Item>
          </DropdownList>
        )}
      </div>
    );
  };

  renderInput = (col, index) => {
    const { row, options, onCreateOptions } = this.props;
    const { errors, inputValues } = this.state;

    if (!this.isColumnEditable(col)) {
      return <span className={styles.DataTable__col_inner} />;
    }

    if (col.type === 'select') {
      const selectOptions = options[col.selectOptions];
      const onCreateOption = onCreateOptions[col.selectOptions];

      const onChange = option => this.onInputChange(col, !option ? null : option.value);
      const defaultValue =
        col.accessorExpand && inputValues[col.accessor] && row[col.accessorExpand]
          ? { value: row[col.accessorExpand].id, label: row[col.accessorExpand].name }
          : null;
      const value =
        selectOptions.find(({ value }) => value === inputValues[col.accessor]) || defaultValue;

      return (
        <Select
          name="contractor"
          placeholder="выбрать"
          value={value}
          options={selectOptions}
          isClearable
          theme="noBorder"
          backgroundColor={errors[col.accessor] ? 'rgba(255, 0, 0, 0.1)' : 'transparent'}
          creatable={col.selectCreatable}
          onCreateOption={col.selectCreatable ? onCreateOption(onChange) : undefined}
          onChange={onChange}
          autoFocus={row.clickedElement === col.accessor}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
        />
      );
    }
    const isAutoComplete = this.isColumnAutoComplete(col);
    let format = formatToText;
    if (col.type === 'number') {
      format = formatToPrice;

      if (col.maxValue) {
        format.numeralIntegerScale = col.maxValue;
      }
    }

    const InputComponent = isAutoComplete ? AutocompleteInput : CleaveInput;

    return (
      <Fragment>
        <InputComponent
          style={errors[col.accessor] ? { backgroundColor: 'rgba(255, 0, 0, 0.1)' } : {}}
          autoFocus={row.clickedElement === col.accessor || (!row.clickedElement && col.accessorOut === 'label_name')}
          options={format}
          name={`${col.accessor}`}
          placeholder={col.placeholder && col.placeholder(row)}
          type="text"
          className={styles.DataTable__input_col}
          value={inputValues[col.accessor]}
          onChange={e => this.onInputChange(col, e.target.value)}
          autocomplete="off"
          autocompleteOptions={isAutoComplete ? options[col.autocompleteOptions] : undefined}
          onAutocompleteOptionClick={isAutoComplete ? this.onAutocompleteOptionClick : undefined}
        />
        <span style={{ marginLeft: 3, position: 'relative', left: -10, color: 'lightgray' }}>
          {col.unit}
        </span>
      </Fragment>
    );
  };

  render() {
    const { columns, row, edit, removeRow, onExitEditing } = this.props;

    return (
      <form onSubmit={this.onSubmit} className={styles.DataTable__form}>
        <div
          className={cn(styles.DataTable__row, styles.DataTable__row_editing)}
          data-level={row.level}
        >
          {columns.map((col, colIndex) => {
            return (
              <div
                key={`editor_${colIndex}'`}
                className={cn(
                  styles.DataTable__col,
                  col.type === 'select' && styles.DataTable__col_select,
                )}
                data-number={
                  colIndex === 0 ? row.label_name && row.label_name.split(' ')[0] : undefined
                }
                data-level={row.level}
              >
                {colIndex === 0 && row.level === 2 && this.renderBadge(row)}
                {this.renderInput(col, colIndex)}
              </div>
            );
          })}

          {(edit || !row.id) && (
            <div style={{ position: 'absolute', right: '0', height: '40px' }}>
              <Button
                type="button"
                theme="basket"
                onClick={e => {
                  if (!row.id) {
                    onExitEditing();
                    return;
                  }
                  removeRow(e, row);
                  e.stopPropagation();
                }}
                icon="basket"
              />
            </div>
          )}
        </div>
        <input type="submit" style={{ display: 'none' }} />
      </form>
    );
  }
}

export default onClickOutside(EditingRow);
