import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import createHTML5Backend from 'react-dnd-html5-mixed-backend/src';
import { DragDropContext } from 'react-dnd';
import get from 'lodash/get';
import isFunction from 'lodash/isFunction';
import map from 'lodash/map';
import last from 'lodash/last';
import omit from 'lodash/omit';
import isEqual from 'lodash/isEqual';
import Button from '../elements/button';
import CostMark from '../elements/costMark';
import DropdownList from '../elements/dropdownList';
import Accordion from '../elements/accordion';
import { PAYMENT, MATERIAL, MECHANISM, INVOICE } from '../../api/resource-types';
import NormalRow from './normal-row';
import EditingRow from './editing-row';

import styles from './styles.less';

class DataTable extends Component {
  static propTypes = {
    className: PropTypes.string,
    editable: PropTypes.bool,
    levels: PropTypes.number,
    transferBetweenLevels: PropTypes.array,
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        content: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
        accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
      }),
    ).isRequired,
    rows: PropTypes.arrayOf(
      PropTypes.shape({
        rowId: PropTypes.number.isRequired,
        children: PropTypes.shape({
          rowId: PropTypes.number.isRequired,
        }),
      }),
    ),
    options: PropTypes.object,
    onCreateOptions: PropTypes.object,
    onDragRow: PropTypes.func,
    onDropRow: PropTypes.func,
    onExitEditing: PropTypes.func,
    emptyCellContent: PropTypes.string,
    addBtnText: PropTypes.func,
    commitRowChanges: PropTypes.func,
    removeRow: PropTypes.func,
    isTrial: PropTypes.bool,
    openTariffAlert: PropTypes.func,
    getValueFromAutocompleteOption: PropTypes.func,
    theme: PropTypes.oneOf(['estimate', 'valuation']),
  };

  static defaultProps = {
    className: undefined,
    editable: false,
    levels: 3,
    transferBetweenLevels: [false, false, false],
    rows: [],
    options: {},
    isTrial: false,
    onCreateOptions: {},
    onDragRow: () => {},
    onDropRow: () => {},
    onExitEditing: () => {},
    emptyCellContent: '',
    addBtnText: () => 'Добавить',
    commitRowChanges: () => {},
    removeRow: () => {},
    openTariffAlert: () => {},
    getValueFromAutocompleteOption: () => {},
    theme: 'estimate',
  };

  state = {
    lastScrollY: 0,
    fixed: false,
    dropdown: null,
    editingRowItem: null,
    editingRow: null,
    tasksExpand: {},
    updatedRow: null
  };

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    document.addEventListener('keydown', this.handleDocumentKeyDown);

    document.addEventListener('scroll', this.handleScroll);
    if (this.tableHeader) {
      this.setState({ lastScrollY: this.tableHeader.offsetTop });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
    document.removeEventListener('keydown', this.handleDocumentKeyDown);
    document.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    const { lastScrollY, fixed } = this.state;
    const scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    if (scrollTop > lastScrollY && !fixed) {
      this.setState({ fixed: true });
    } else if (scrollTop < lastScrollY && fixed) {
      this.setState({ fixed: false });
    }
  };

  handleClickOutside = e => {
    if (
      this.dropdown &&
      !this.dropdown.el.contains(e.target) &&
      (!this.addButton || !this.addButton.contains(e.target))
    ) {
      this.toggleDropdown(null);
    }
  };

  handleDocumentKeyDown = e => {
    if (e.key === 'Escape') {
      this.onExitEditing();
    }
  };

  onExitEditing = isSubmitExit => {
    const { onExitEditing } = this.props;

    this.setState({updatedRow: {}});
    this.setState({updatedRow: {...this.state.editingRow, ...this.state.editingRowItem}});
    this.setState({
      editingRow: null,
      dropdown: null,
      editingRowItem: null,
    });
    onExitEditing(isSubmitExit);
  };

  onSubmitEditing = () => {
    const { editingRow, editingRowItem } = this.state;
    const { commitRowChanges } = this.props;

    if (Object.keys(editingRow).length > 0) {
      commitRowChanges({
        ...editingRowItem,
        ...editingRow,
      });
    }
    this.onExitEditing(true);
  };

  onDataChange = (name, value) => {
    this.setState(prevState => ({
      editingRow: {
        ...prevState.editingRow,
        [name]: value,
      },
    }));
  };

  handleSetType = (type, row) => {
    this.onDataChange('type', type);

    this.setState({
      dropdown: null,
      editingRowItem: { level: row.level, parentId: row.parentId },
    });
  };

  startAddingRow = (e, row) => {
    const { editingRow } = this.state;
    const { isTrial } = this.props;
    if (isTrial) {
      this.props.openTariffAlert();
    } else {
      if (editingRow !== null) return;

      this.addButton = e.target;
      if (row.level === 2) {
        this.toggleDropdown(row);
      } else {
        this.setState({
          editingRowItem: { level: row.level, parentId: row.parentId },
          editingRow: this.initEditingRow(row),
        });
      }
    }
  };

  startEditingRow = row => {
    const { editingRow } = this.state;
    const { editable } = this.props;

    if (editingRow !== null || !editable || row.uneditable) return;

    this.setState({
      editingRowItem: {
        id: row.id,
        parentId: row.parentId,
        level: row.level,
      },
      editingRow: {},
    });
  };

  onRemoveRow = (e, row) => {
    const { removeRow } = this.props;
    this.setState({
      dropdown: null,
      editingRowItem: null,
      editingRow: null,
    });

    removeRow(e, row);
  };

  toggleDropdown = row => {
    this.setState(prevState => ({
      dropdown: !prevState.dropdown
        ? {
            parentId: row.parentId,
            level: row.level,
          }
        : null,
      editingRow: row && this.initEditingRow(row),
      editingRowItem: null,
    }));
  };

  initEditingRow = row => {
    const { columns } = this.props;

    return columns
      .filter(
        column =>
          column.type === 'number' && this.isColumnEditable(column, row) && !column.calculate,
      )
      .map(column => column.accessor)
      .reduce((acc, cur) => {
        acc[cur] = 0;
        return acc;
      }, {});
  };

  isColumnEditable = (col, row) => {
    if (isFunction(col.editable)) {
      return col.editable(row);
    }

    return !!col.editable;
  };

  renderAddButtonRow = row => {
    const { dropdown } = this.state;
    const { addBtnText } = this.props;

    let btnText;
    const { level } = row;
    if (isFunction(addBtnText)) {
      btnText = addBtnText(level);
    }

    return (
      <Fragment>
        <Button
          className={cn(styles.DataTable__button, {
            [styles.DataTable__button_stage]: level === 0,
            [styles.DataTable__button_work]: level === 1,
            [styles.DataTable__button_resource]: level === 2,
          })}
          type="button"
          theme="add"
          onClick={e => this.startAddingRow(e, row)}
          icon={level === 2 ? 'plusSquare' : 'plus-sm'}
        >
          {btnText}
        </Button>
        {dropdown && dropdown.parentId === row.parentId && dropdown.level === row.level && (
          <DropdownList
            ref={el => {
              this.dropdown = el;
            }}
            style={{ whiteSpace: 'nowrap' }}
            open={isEqual(dropdown, row)}
          >
            <DropdownList.Item onClick={() => this.handleSetType(PAYMENT, row)}>
              <CostMark stateName="ТРД" />
              Оплата труда
            </DropdownList.Item>
            <DropdownList.Item onClick={() => this.handleSetType(MATERIAL, row)}>
              <CostMark stateName="МАТ" />
              Материалы
            </DropdownList.Item>
            <DropdownList.Item onClick={() => this.handleSetType(MECHANISM, row)}>
              <CostMark stateName="МЕХ" />
              Механизмы
            </DropdownList.Item>
            <DropdownList.Item onClick={() => this.handleSetType(INVOICE, row)}>
              <CostMark stateName="НКЛ" />
              Накладные
            </DropdownList.Item>
          </DropdownList>
        )}
      </Fragment>
    );
  };

  renderControlsRow = row => {
    const {
      editable,
      columns,
      options,
      onCreateOptions,
      getValueFromAutocompleteOption,
    } = this.props;
    const { editingRow, editingRowItem } = this.state;

    if (!row || !editable) {
      return null;
    }

    if (
      editingRowItem &&
      !editingRowItem.id &&
      editingRowItem.parentId === row.parentId &&
      editingRowItem.level === row.level
    ) {
      return (
        <EditingRow
          row={{ ...editingRowItem, ...editingRow }}
          columns={columns}
          options={options}
          onCreateOptions={onCreateOptions}
          onSubmitEditing={this.onSubmitEditing}
          onDataChange={this.onDataChange}
          onExitEditing={this.onExitEditing}
          getValueFromAutocompleteOption={getValueFromAutocompleteOption}
        />
      );
    }

    return this.renderAddButtonRow(row);
  };

  isArrowOpen = () => {
    const { tasksExpand } = this.state;
    const { rows, levels } = this.props;

    if (levels === 3) {
      return rows.every(stage =>
        stage.children.every(task =>
          tasksExpand[task.id] === undefined ? task.isNew : tasksExpand[task.id],
        ),
      );
    }

    return rows.every(row => (tasksExpand[row.id] === undefined ? row.isNew : tasksExpand[row.id]));
  };

  onHover = (propsHovered, monitorItem) => {
    const { transferBetweenLevels } = this.props;

    if (
      propsHovered.level !== 1 ||
      monitorItem.draggingItemLevel !== 2 ||
      propsHovered.level === monitorItem.draggingItemLevel ||
      propsHovered.row.id === monitorItem.draggingRowParentId ||
      !transferBetweenLevels[1]
    ) {
      return;
    }

    const taskId = propsHovered.row.id;

    this.setState(prevState => {
      if (prevState.tasksExpand[taskId]) {
        return null;
      }
      return { tasksExpand: { ...prevState.tasksExpand, [taskId]: true } };
    });
  };

  onAccordionClick = (rowId, prevIsOpen) => {
    this.setState(prevState => ({
      tasksExpand: {
        ...prevState.tasksExpand,
        [rowId]: !prevIsOpen,
      },
    }));
  };

  onArrowClick = () => {
    const { rows, levels } = this.props;
    const tasksExpand = {};

    const open = this.isArrowOpen();

    if (levels === 3) {
      rows.forEach(item =>
        item.children.forEach(child => {
          tasksExpand[child.id] = !open;
        }),
      );
    } else {
      rows.forEach(row => {
        tasksExpand[row.id] = !open;
      });
    }

    this.setState({
      tasksExpand,
    });
  };

  renderRow = (row, level = 0) => {
    const { editingRowItem, editingRow, tasksExpand, updatedRow } = this.state;
    const {
      columns,
      transferBetweenLevels,
      emptyCellContent,
      editable,
      options,
      onDragRow,
      onDropRow,
      onCreateOptions,
      getValueFromAutocompleteOption,
      theme,
      openTariffAlert,
      isTrial,
    } = this.props;

    const isEditing =
      editingRowItem && editingRowItem.id === row.id && editingRowItem.level === row.level;
    const isExpand = tasksExpand[row.id] === undefined ? row.isNew : tasksExpand[row.id];

    const innerContent = () => (
      <Fragment>
        {map(row.children, childRow => this.renderRow(childRow, level + 1))}
        {level < 2 &&
          this.renderControlsRow({
            parentId: get(last(row.children), 'parentId', row.id),
            level: level + 1,
          })}
      </Fragment>
    );

    if(updatedRow && updatedRow.id === row.id && updatedRow.level === row.level) {
      const omitProps = [
        'id',
        'parentId',
        'level',
        'isLastChild',
        'open',
        'children',
        'rowId',
        'parentRowId',
      ];

      const updatedRowKeys = Object.keys(omit(updatedRow, omitProps));

      updatedRowKeys.forEach((index) => {
        const column = columns.find(col => col.accessor === index);
        const columnAccessor = column ? column.accessor : index

        if(index === 'name' && row[column.accessorOut]) {
          row[column.accessorOut] = row[column.accessorOut].split(' ')[0]+' '+updatedRow[index];
        }

        if(column && column.selectOptions && options[column.selectOptions]) {
          const option = options[column.selectOptions].find(opt => opt.value === updatedRow[index]);
          row[column.accessorOut] = option.label;
        }

        row[columnAccessor] = updatedRow[index];
      })
    }

    const renderContent = () => {
      if (level === 0) {
        return <div data-wrapper>{innerContent()}</div>;
      }
      if (level === 1 && row.rowId) {
        return (
          <Accordion
            className={cn(
              styles.DataTable__Accordion,
              level === 1 && styles.DataTable__Accordion_resources,
            )}
            rowId={row.id}
            isOpen={isExpand}
            onAccordionClick={this.onAccordionClick}
            theme={theme}
          >
            {innerContent()}
          </Accordion>
        );
      }
      return innerContent();
    };

    return (
      <Fragment key={row.rowId}>
        {isEditing && editable ? (
          <EditingRow
            row={{ ...row, ...editingRow }}
            columns={columns}
            options={options}
            onCreateOptions={onCreateOptions}
            removeRow={this.onRemoveRow}
            onSubmitEditing={this.onSubmitEditing}
            onDataChange={this.onDataChange}
            onExitEditing={this.onExitEditing}
            getValueFromAutocompleteOption={getValueFromAutocompleteOption}
            edit
          />
        ) : (
          <NormalRow
            level={level}
            transferBetweenLevels={transferBetweenLevels}
            onDragRow={onDragRow}
            onDropRow={onDropRow}
            onHover={this.onHover}
            canDrag={editable}
            row={row}
            isTrial={isTrial}
            openTariffAlert={openTariffAlert}
            columns={columns}
            emptyCellContent={emptyCellContent}
            startEditing={this.startEditingRow}
            onAccordionClick={this.onAccordionClick}
            tasksExpand={tasksExpand}
          />
        )}
        {renderContent()}
      </Fragment>
    );
  };

  render() {
    const { className, levels, columns, rows, theme } = this.props;
    const { fixed } = this.state;
    const startLevel = 3 - levels;

    return (
      <div
        ref={el => {
          this.tableHeader = el;
        }}
        className={cn(styles.DataTable, styles[`DataTable_theme_${theme}`], className)}
      >
        <div
          className={cn(
            styles.DataTable__row,
            styles.DataTable__row_header,
            fixed && styles.DataTable__row_fix,
          )}
          role="button"
          tabIndex={0}
          onClick={this.onArrowClick}
        >
          {columns.map(col => {
            if (col.accessor === 'name') {
              return (
                <div
                  role="button"
                  tabIndex={0}
                  onClick={this.onArrowClick}
                  key={col.accessor}
                  className={cn(
                    styles.DataTable__col,
                    col.className,
                    styles.DataTable__col_collapsed,
                    this.isArrowOpen() && styles.DataTable__col_opened,
                  )}
                >
                  {col.content}
                </div>
              );
            }
            return (
              <div key={col.accessor} className={cn(styles.DataTable__col, col.className)}>
                {col.content}
              </div>
            );
          })}
        </div>
        {rows.map(row => this.renderRow(row, startLevel))}
        {this.renderControlsRow({ level: startLevel })}
      </div>
    );
  }
}

export default DragDropContext(createHTML5Backend)(DataTable);
