import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { DragSource, DropTarget } from 'react-dnd';
import flow from 'lodash/flow';
import CostMark from '../../elements/costMark';
import { formatNumber } from '../../../utils/helpers';

import styles from '../styles.less';

const ITEM_TYPE = 'ITEM';

const itemSource = {
  beginDrag(props) {
    const { level, row } = props;

    return {
      draggingItemLevel: level,
      draggingRowId: row.id,
      draggingRowNumber: row.number,
      draggingRowParentId: level !== 0 ? row.parentId : undefined,
      draggingRowNewParentId: level !== 0 ? row.parentId : undefined,
    };
  },

  isDragging(props, monitor) {
    return (
      monitor.getItem().draggingRowId === props.row.id &&
      monitor.getItem().draggingItemLevel === props.level
    );
  },

  endDrag(props, monitor) {
    const { onDropRow, level } = props;
    const { draggingRowId, draggingRowParentId, draggingRowNewParentId } = monitor.getItem();

    onDropRow({
      level,
      dragId: draggingRowId,
      dragParentId: draggingRowParentId,
      dragNewParentId: draggingRowNewParentId,
    });
  },
};

function collectSource(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  };
}

const itemTarget = {
  hover(props, monitor, component) {
    const { onDragRow, onHover, row } = props;
    onHover(props, monitor.getItem());

    if (!component) return null;
    if (!monitor.canDrop()) return null;

    const {
      draggingItemLevel,
      draggingRowId,
      draggingRowNumber,
      draggingRowNewParentId,
    } = monitor.getItem();
    const { id: hoverId, level, number: hoverNumber, parentId: hoverParentId } = row;

    if (draggingRowId === hoverId) return null;

    // eslint-disable-next-line
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
    const clientOffset = monitor.getClientOffset();
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    if (draggingItemLevel === level) {
      // Dragging downwards
      if (draggingRowNumber < hoverNumber && hoverClientY < hoverMiddleY) return null;

      // Dragging upwards
      if (draggingRowNumber > hoverNumber && hoverClientY > hoverMiddleY) return null;

      onDragRow(draggingItemLevel, draggingRowId, hoverId);
      // eslint-disable-next-line
      monitor.getItem().draggingRowNumber = hoverNumber;
      // eslint-disable-next-line
      monitor.getItem().draggingRowNewParentId = hoverParentId;
    } else {
      if (draggingRowNumber === 0 && draggingRowNewParentId === hoverId) return null;

      onDragRow(draggingItemLevel, draggingRowId, hoverId, level);
      // eslint-disable-next-line
      monitor.getItem().draggingRowNumber = 0;
      // eslint-disable-next-line
      monitor.getItem().draggingRowNewParentId = hoverId;
    }

    return {};
  },

  canDrop(props, monitor) {
    const { draggingItemLevel, draggingRowParentId } = monitor.getItem();
    const { level, transferBetweenLevels, row } = props;

    switch (level) {
      case 2:
      case 1: {
        if (draggingItemLevel === level) {
          return (
            draggingRowParentId === row.parentId ||
            (draggingRowParentId !== row.parentId && transferBetweenLevels[draggingItemLevel])
          );
        }
        if (draggingItemLevel === 2 && level === 1) {
          return true;
        }

        return false;
      }
      case 0:
        return draggingItemLevel === level || draggingItemLevel === 1;
      default:
        return false;
    }
  },
};

function collectTarget(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  };
}

class NormalRow extends Component {
  /* eslint-disable */
  static propTypes = {
    canDrag: PropTypes.bool.isRequired,
    isDragging: PropTypes.bool.isRequired,
    connectDragSource: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    level: PropTypes.number.isRequired,
    transferBetweenLevels: PropTypes.array.isRequired,
    row: PropTypes.object.isRequired,
    columns: PropTypes.array.isRequired,
    emptyCellContent: PropTypes.string.isRequired,
    startEditing: PropTypes.func.isRequired,
    onAccordionClick: PropTypes.func.isRequired,
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    openTariffAlert: PropTypes.func,
    isTrial: PropTypes.bool,
    tasksExpand: PropTypes.object.isRequired,
  };

  static defaultProps = {
    openTariffAlert: () => {},
    isTrial: false,
  };
  /* eslint-enable */

  renderCell = (col, index) => {
    const { row, emptyCellContent } = this.props;

    const isTypeCell = index === 0 && row.level === 2;
    const rowColAccessorOut = row[col.accessorOut];
    const rowColAccessor = row[col.accessor];
    let text;

    if (rowColAccessorOut != null || rowColAccessor != null) {
      if (col.type === 'number') {
        text = `${(rowColAccessorOut && formatNumber(rowColAccessorOut)) ||
          (rowColAccessor && formatNumber(rowColAccessor))}`;
      } else {
        text = `${rowColAccessorOut || rowColAccessor}`;
      }
      text += col.unit ? ` ${col.unit}` : '';
    } else {
      text = emptyCellContent;
    }
    return (
      <div
        key={row.rowId + col.accessor}
        className={cn(
          styles.DataTable__col,
          col.type === 'select' && styles.DataTable__col_select,
          styles['DataTable__col_not-editable']
        )}
      >
        <div className={cn(styles.DataTable__col_inner, col.className && styles[col.className])}>
          {isTypeCell && <CostMark type={row.type} />}
          <span onClick={((e) => this.onRowClick(e, col.accessor))} className={cn(styles.DataTable__col_name)}>{text}</span>
        </div>
      </div>
    );
  };

  onRowClick = (e, accessor) => {
    const { row, startEditing, openTariffAlert, isTrial, tasksExpand, onAccordionClick } = this.props;
    if (isTrial) {
      openTariffAlert();
    } else {
      if(accessor) row.clickedElement = accessor;
      (tasksExpand[row.id] === undefined || tasksExpand[row.id] === false) && row.level !== 0 ? onAccordionClick(row.id) : startEditing(row);
    }
  };

  render() {
    const {
      canDrag,
      isDragging,
      connectDragSource,
      connectDragPreview,
      connectDropTarget,
      row,
      columns,
    } = this.props;
    const element = (
      <div
        role="button"
        tabIndex={0}
        data-level={row.level}
        className={cn(styles.DataTable__row, !row.uneditable && styles.DataTable__row_pointer, row.level === 0 && styles.DataTable__col_headerName)}
        onClick={this.onRowClick}
      >
        {columns.map(this.renderCell)}
      </div>
    );

    return connectDropTarget(
      connectDragPreview(
        <div className={cn(styles.dnd_wrapper, !row.uneditable && styles.DataTable__row_estimate)} data-invisible={isDragging ? true : undefined}>
          {canDrag && connectDragSource(<div className={styles.dnd__item} />)}
          {element}
        </div>,
      ),
    );
  }
}

export default flow(
  DragSource(ITEM_TYPE, itemSource, collectSource),
  DropTarget(ITEM_TYPE, itemTarget, collectTarget),
)(NormalRow);
